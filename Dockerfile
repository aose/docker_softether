FROM alpine:3.4
ENV LANG=C.UTF-8

#See also
#https://github.com/frol/docker-alpine-glibc

WORKDIR /usr/local

RUN ALPINE_GLIBC_BASE_URL="https://github.com/sgerrand/alpine-pkg-glibc/releases/download" && \
    ALPINE_GLIBC_PACKAGE_VERSION="2.23-r3" && \
    ALPINE_GLIBC_BASE_PACKAGE_FILENAME="glibc-$ALPINE_GLIBC_PACKAGE_VERSION.apk" && \
    ALPINE_GLIBC_BIN_PACKAGE_FILENAME="glibc-bin-$ALPINE_GLIBC_PACKAGE_VERSION.apk" && \
    ALPINE_GLIBC_I18N_PACKAGE_FILENAME="glibc-i18n-$ALPINE_GLIBC_PACKAGE_VERSION.apk" && \
	\
    apk add --no-cache \
		ca-certificates build-base git wget readline ncurses readline-dev openssl-dev ncurses-dev  && \
	\
    wget \
        "https://raw.githubusercontent.com/andyshinn/alpine-pkg-glibc/master/sgerrand.rsa.pub" \
        -O "/etc/apk/keys/sgerrand.rsa.pub" && \
    wget \
        "$ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/$ALPINE_GLIBC_BASE_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/$ALPINE_GLIBC_BIN_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/$ALPINE_GLIBC_I18N_PACKAGE_FILENAME" && \
    apk add --no-cache \
        "$ALPINE_GLIBC_BASE_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_BIN_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_I18N_PACKAGE_FILENAME" && \
    \
    rm "/etc/apk/keys/sgerrand.rsa.pub" && \
    /usr/glibc-compat/bin/localedef --force --inputfile POSIX --charmap UTF-8 C.UTF-8 || true && \
    echo "export LANG=C.UTF-8" > /etc/profile.d/locale.sh && \
    \
    rm \
        "$ALPINE_GLIBC_BASE_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_BIN_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_I18N_PACKAGE_FILENAME" && \
	\
    git clone https://github.com/SoftEtherVPN/SoftEtherVPN.git && \
	cd SoftEtherVPN && \
    cp src/makefiles/linux_64bit.mak Makefile && \
    LDFLAGS="-L/usr/local/lib/ -liconv" CCFLAGS="-I/usr/local/include/" make && make install && \
	cd ../ && \
	rm -rf SoftEtherVPN && \
	\
    apk del --purge \
		glibc-i18n wget git build-base readline-dev openssl-dev ncurses-dev

WORKDIR /usr/vpnserver

ADD runner.sh /usr/vpnserver/runner.sh
RUN chmod +x runner.sh

#EXPOSE 1701/udp 500/udp 4500/udp 992/tcp

ENTRYPOINT ["/usr/vpnserver/runner.sh"]
