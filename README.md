docker_softether
====

Docker container for [Softether VPN](https://softether.org/).


## Usage

    $ docker-compose build
    $ docker-compose up -d

If you've already vpn_server.config, add same directory.

    $ cp vpn_server.config /path/to/DOCKERFILE_DIR
    $ docker-compose build
    $ docker-compose up -d


## Author

[i_aose](https://twitter.com/i_aose)
